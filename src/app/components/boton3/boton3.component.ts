import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/servicios/data.service';

@Component({
  selector: 'app-boton3',
  templateUrl: './boton3.component.html',
  styleUrls: ['./boton3.component.css']
})
export class Boton3Component implements OnInit {

  textoBoton3: string = 'Hola Botón 3!'
  constructor(public dataServicio: DataService) { }

  ngOnInit(): void {
  }

  cambiarDato(): void{
    this.dataServicio.texto = this.textoBoton3
  }

}
